function openNav() {
    document.getElementById("mySidenav").style.width = "200px";
}

$(document).mouseup(function (e) {
    const containerA = $(".sidenav");

    // if the target of the click isn't the container nor a descendant of the container
    if (!containerA.is(e.target) && containerA.has(e.target).length === 0) {
        document.getElementById("mySidenav").style.width = "0";
    }
});


var headersCon = ['Welcome', 'Some', 'Awesome', 'Words', 'Goes', 'Here'];
var textsCon = [
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent et hendrerit magna. In ac mattis orci. Aliquam condimentum consequat sapien, sed aliquam lacus hendrerit in. Integer laoreet tristique suscipit. Suspendisse mi mauris, ultrices id risus quis, sollicitudin auctor lorem. Pellentesque dapibus ac nibh et elementum. Curabitur laoreet nisi tempus enim ornare bibendum. Integer a quam interdum, pretium sem non, aliquet est. Suspendisse lorem turpis, tempus nec tempus et, ullamcorper sed ante. Morbi efficitur velit sapien. Vivamus faucibus lorem sit amet blandit auctor. Donec feugiat metus vitae pulvinar dapibus. Fusce lacinia accumsan lacinia. Suspendisse et gravida nunc. Suspendisse placerat dui bibendum erat pharetra pretium. Fusce mollis mollis risus, eu imperdiet lorem. Proin enim libero, cursus vitae viverra vel, tempus eu elit. Phasellus tristique malesuada dolor at tempor. Etiam sed pharetra ipsum. Phasellus sed iaculis quam, eu consequat lectus. Proin interdum sapien lorem, at viverra metus iaculis vel. Proin id felis ac purus scelerisque mollis a ut felis.",
    'Section 1 text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent et hendrerit magna. In ac mattis orci. ',
    'Section 2 text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent et hendrerit magna. In ac mattis orci. ',
    'Section 3 text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent et hendrerit magna. In ac mattis orci. ',
    'Section 4 text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent et hendrerit magna. In ac mattis orci. ',
    'Section 5 text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent et hendrerit magna. In ac mattis orci. '
];
var imageCon = 
[
"https://images.pexels.com/photos/1086584/pexels-photo-1086584.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
"https://images.pexels.com/photos/544954/pexels-photo-544954.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
"https://images.pexels.com/photos/274054/pexels-photo-274054.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
"https://images.pexels.com/photos/994473/pexels-photo-994473.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
"https://images.pexels.com/photos/1297651/pexels-photo-1297651.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
"https://images.pexels.com/photos/978342/pexels-photo-978342.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940"
];

function changeDescription(clicked_id) {
    $('#headerContent').fadeOut( "slow", function() {
        document.getElementById("headerContent").innerHTML = headersCon[clicked_id];
        $(this).delay(100).fadeIn();
    });
    $('#textContent').fadeOut( "slow", function() {
        document.getElementById("textContent").innerHTML = textsCon[clicked_id];
        $(this).delay(100).fadeIn();
    });
    $('#imageContent').fadeOut( "slow", function() {
        document.getElementById("imageContent").src = imageCon[clicked_id];
        $(this).delay(100).fadeIn();
    });
    document.getElementById("mySidenav").style.width = "0";
}